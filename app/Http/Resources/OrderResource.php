<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'price' => $this->price,
            'time' => $this->time . ' hours',
            'customers' => new UserResource($this->user),
            'car' => new CarResource($this->car),
        ];
    }
}
