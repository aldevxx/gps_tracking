<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'operators'
        ]);
        Permission::create([
            'name' => 'drivers'
        ]);
        Permission::create([
            'name' => 'users'
        ]);

        $operators = Role::where('name', 'operators')->first();
        $operators->permissions()->attach([1]);
        $drivers = Role::where('name', 'drivers')->first();
        $drivers->permissions()->attach([2]);
        $users = Role::where('name', 'users')->first();
        $users->permissions()->attach([3]);
    }
}
