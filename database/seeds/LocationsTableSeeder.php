<?php

use App\Location;
use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::create([
            'user_id' => 2,
            'longitude' => 112.78,
            'latitude' => -7.63,
        ]);
    }
}
