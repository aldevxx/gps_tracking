<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Gohan',
            'password' => bcrypt('12345678'),
            'email' => 'gohan@gmail.com',
            'phone' => '081230002558',
            'photo' => 'Gusion_V.E.N.O.M._Emperor_Scorpion_wall.png',
            'role_id' => 1,
        ]);
        User::create([
            'name' => 'Hanimu',
            'password' => bcrypt('12345678'),
            'email' => 'hanimu@gmail.com',
            'phone' => '081230002558',
            'photo' => 'Gusion_V.E.N.O.M._Emperor_Scorpion_wall.png',
            'role_id' => 2,
        ]);
        User::create([
            'name' => 'Markonam',
            'password' => bcrypt('12345678'),
            'email' => 'markonam@gmail.com',
            'phone' => '081230002558',
            'photo' => 'Gusion_V.E.N.O.M._Emperor_Scorpion_wall.png',
            'role_id' => 3,
        ]);
    }
}
